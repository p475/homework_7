# homework 1
my_tuple = [1, 1, 3, 9, 4, 1, 5, 3]
not_duplicate = []
for i in my_tuple:
    if not i in not_duplicate:
        not_duplicate.append(i)

print(not_duplicate)

# homework 1.1

a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
c = []
for items in a:
    for numbers in b:
        if items == numbers:
            if not items in c:
                c.append(items)
print(c)

# homework 2

a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = []
for i in a:
    if i % 5 == 0:
        b.append(i)

print(b)


# homework 3

def reverse_last(string: str) -> str:
    words = string.split()
    result = []
    for word in words:
        result.insert(0, word)
    return " ".join(result)


print(reverse_last("My name is Edgar"))

# homework 4

my_tuple_1 = (1, 8, 6, 9)


def reverse1(list_: tuple) -> tuple:
    return list_[::-1]


print(reverse1(my_tuple_1))

# homework 5

sort_ = [1, 8, 7, 2, 6, 4, 2]


def sort_asc(lists: list) -> list:
    max_list = []
    while lists:
        max = lists[0]
        for j in lists:
            if max < j:
                max = j

        max_list.append(max)
        lists.remove(max)

    return max_list
print(sort_asc(sort_))

# homework 6

lists_1 = [1, 8, 7, 2, 6, 4, 2]

new_list_1 = sort_asc(lists_1)

print(new_list_1[1])
